'use strict';

// import vue from 'vue'
// import 'socket.io'
// import axios from 'axios'
var host = location.hostname;
var socket = io.connect('http://192.168.252.126:3000');

var app = new Vue({
    el: '#app',
    data: {
        users: []
    },
    methods: {
        init: function init() {
            axios.get('https://ipinfo.io/').then(function (e) {
                socket.emit('newUser', e.data);
                // console.log(e.data)
            });
        },
        insertData: function insertData(e) {
            this.users.push(e);
        }
    },
    created: function created(argument) {
        this.init();
    }
});

socket.on('newUserData', function (data) {
    app.insertData(data);
    console.log(data);
});

var $ = function $(e) {
    return document.getElementById(e);
};

// socket.on('newUser', function(e) {
// 	console.log(e)
// })


$('send').addEventListener('click', function () {
    // alert()
    // console.log(socket.emit('welcome', 'El click funciona'))
});