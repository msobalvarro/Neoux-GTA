var express = require('express'),
    http = require('http'),
    app = express(),
    server = app.listen(3000, function(error) {
        (error) ? console.log('Error'): console.log('Servidor corriendo')
    }),
    io = require('socket.io').listen(server)


// configuraciones de app
app.use(express.static('public'))

// enrutamiento
app.get('/', function(req, res) {
    res.send('/')
})

// socket.IO
io.on('connect', function(socket) {

    socket.on('newUser', function(e) {
    	newUserNot(e)
    })

    // socket.on('')
})

function newUserNot(msg) {
    io.emit('newUserData', msg)
}